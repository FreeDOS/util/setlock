;
;   FREEDOS: Alternate MS-DOS compatable operating system
;   Copyright (C) 1996, FreeDOS programming Team
;
;   This program is free software; you can redistribute it and/or modify
;   it under the terms of the GNU General Public License as published by
;   the Free Software Foundation; either version 1, or (at your option)
;   any later version.
;
;   This program is distributed in the hope that it will be useful,
;   but WITHOUT ANY WARRANTY; without even the implied warranty of
;   MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
;   GNU General Public License for more details.
;
;   You should have received a copy of the GNU General Public License
;   along with this program; if not, write to the Free Software
;   Foundation, Inc., 675 Mass Ave, Cambridge, MA 02139, USA.
;

title SetLock Utility

code segment
    assume          cs:code,ds:code

    org 100h

code_start:

    mov bx,0080h            ; Any args ?
    mov al,[bx]
    cmp al,0
    jne args_present
        jmp print_help

args_present:               ; Check for valid args

    mov si,offset caps_off
    mov bx,0080h
    call search_arg
    cmp al,0ffh
    jne skip_1
        mov ax,0040h
        push ax
        pop es
        and byte ptr es:[0017h],0bfh
skip_1:
    mov si,offset caps_on
    mov bx,0080h
    call search_arg
    cmp al,0ffh
    jne skip_2
        mov ax,0040h
        push ax
        pop es
        or byte ptr es:[0017h],040h
skip_2:
    mov si,offset num_off
    mov bx,0080h
    call search_arg
    cmp al,0ffh
    jne skip_3
        mov ax,0040h
        push ax
        pop es
        and byte ptr es:[0017h],0dfh
skip_3:
    mov si,offset num_on
    mov bx,0080h
    call search_arg
    cmp al,0ffh
    jne skip_4
        mov ax,0040h
        push ax
        pop es
        or byte ptr es:[0017h],020h
skip_4:
    mov si,offset scroll_off
    mov bx,0080h
    call search_arg
    cmp al,0ffh
    jne skip_5
        mov ax,0040h
        push ax
        pop es
        and byte ptr es:[0017h],0efh
skip_5:
    mov si,offset scroll_on
    mov bx,0080h
    call search_arg
    cmp al,0ffh
    jne skip_6
        mov ax,0040h
        push ax
        pop es
        or byte ptr es:[0017h],010h
skip_6:

code_done:
    mov ax,4c00h            ; Terminate Programme
    int 21h

; Entry None

print_help:
    mov dx,offset help_message
    mov ah,09h
    int 21h
    jmp code_done

; Entry bx=command line offset
;       si=command line arg
; Exit  al=[0=no match/0ffh=match]

search_arg:
    mov di,si               ; Reset regs
    mov cx,0

search_loop:
    mov al,[bx]             ; Fetch character
    cmp al,0                ; End of command tail ?
    je search_done
    cmp bx,0100h
    je search_done

    and al,05fh             ; Upper case

    mov ah,[di]             ; Fetch arg char
    cmp al,ah
    jne search_next
        inc cx              ; Inc match count
        inc bx              ; Inc counts
        inc di
        mov ah,[di]         ; End of match string ?
        cmp ah,0
        je search_match
        jmp search_loop

search_next:                ; No match reset match count and continue
    cmp cx,0
    jne search_arg

    inc bx
    jmp search_arg

search_match:
    mov al,0ffh             ; Return match found
    retn

search_done:                ; Return search complete
    mov al,0
    retn

caps_on:
    db  'CLON',0
caps_off:
    db  'CLOFF',0
num_on:
    db  'NLON',0
num_off:
    db  'NLOFF',0
scroll_on:
    db  'SLON',0
scroll_off:
    db  'SLOFF',0

help_message:
    db  ' SETLOCK.EXE Version 1.0       Written By S.Hammond' ,0dh ,0ah
    db  ' Set state of capslock and numlock on AT sytle keyboards' ,0dh ,0ah
    db  ' Usage: SETLOCK CL=[ON/OFF]' ,0dh ,0ah
    db  '                NL=[ON/OFF]' ,0dh ,0ah
    db  '                SL=[ON/OFF]' ,0dh ,0ah ,0dh ,0ah,'$'

code ends
    end code_start

